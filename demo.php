<?php

require 'vendor/autoload.php';

define('SHARED', __DIR__ . '/shared');

$userId = $argv[1] ?? null;

if (!is_numeric($userId)) {
    echo 'Invalid USER ID.';
    exit(1);
}

// Instantiate the user hierarchy class
$userHierarchy = new \Polk\UserHierarchy();

// Load the roles and user objects in from json files.
// You can also user setUsers & setRoles methods that accept
// associative arrays.
$userHierarchy->loadFromJSON(
    file_get_contents(SHARED . '/roles.json'),
    file_get_contents(SHARED . '/users.json')
);

// Get a list of all users subordinates
try {
    $users = $userHierarchy->getSubOrdinates($userId);
} catch (Exception $e) {
    echo $e->getMessage();
    exit;
}

// Print the user list as JSON.
print(json_encode($users));

