# Deputy Test - User Hierarchy

###Problem

In our system each user belongs to a user-group with a defined set of permissions.
We name such a group "Role". A certain role (unless it is the root) must have a parent 
role to whom it reports to.

### Setup

To run this application you will require `composer` & `php`

##### Installation

- Clone the repository into a directory of your choice.
```
git clone git@bitbucket.org:polkfarody/deputy-test.git
```

- Run composer install
```
composer install
```

##### Usage
Below is a working version of the code in a file called `demo.php`. This can be run via command line
with the `userId` being passed as the only param.
```
<?php
# demo.php
require 'vendor/autoload.php';

define('SHARED', __DIR__ . '/shared');

$userId = $argv[1] ?? null;

if (!is_numeric($userId)) {
    echo 'Invalid USER ID.';
    exit(1);
}

// Instantiate the user hierarchy class
$userHierarchy = new \Polk\UserHierarchy();

// Load the roles and user objects in from json files.
// You can also user setUsers & setRoles methods that accept
// associative arrays.
$userHierarchy->loadFromJSON(
    file_get_contents(SHARED . '/roles.json'),
    file_get_contents(SHARED . '/users.json')
);

// Get a list of all users subordinates
try {
    $users = $userHierarchy->getSubOrdinates($userId);
} catch (Exception $e) {
    echo $e->getMessage();
    exit;
}

// Print the user list as JSON.
print(json_encode($users));
```
Example:
```
$ php demo.php 1
```
Output:
``` 
[{"Id":4,"Name":"Mary Manager","Role":2},{"Id":3,"Name":"Sam Supervisor","Role":3},{"Id":2,"Name":"Emily Employee","Role":4},{"Id":5,"Name":"Steve Trainer","Role":5}]
```

### What's happening

Once the json or array is loaded into the UserHierarchy object they become User and Role objects
respectfully. When the user calls getSubOrdinates, a method called mapSubOrdinates is called which
sets the `users` property of each User class to the list of users in the role directly beneath. This 
process will run through all of the roles until every user has been updated with their subordinate users.

### Testing

There is not 100% coverage over this application and I mainly focused on the business logic. That
being the main method getSubOrdinates.



