<?php

declare(strict_types=1);

namespace Polk\Tests;

use Polk\DuplicateUserException;
use Polk\User;
use Polk\UserHierarchy;
use PHPUnit\Framework\TestCase;
use Polk\UserInterface;
use Polk\UserNotFoundException;

class UserHierarchyTest extends TestCase {
    /**
     * Test Role Data (This will be converted to an array).
     * @var string
     */
    private static $rolesJson = <<<JSON
[
    {
      "Id": 1,
      "Name": "System Administrator",
      "Parent": 0
    },
    {
      "Id": 2,
      "Name": "Location Manager",
      "Parent": 1
    },
    {
      "Id": 3,
      "Name": "Supervisor",
      "Parent": 2
    },
    {
      "Id": 4,
      "Name": "Employee",
      "Parent": 3
    },
    {
      "Id": 5,
      "Name": "Trainer",
      "Parent": 3
    }
]
JSON;

    /**
     * Test User Data (This will be converted to an array).
     * @var string
     */
    private static $usersJson = <<<JSON
[
     {
        "Id": 1,
        "Name": "Adam Admin",
        "Role": 1
     },
     {
        "Id": 2,
        "Name": "Emily Employee",
        "Role": 4
     },
     {
        "Id": 3,
        "Name": "Sam Supervisor",
        "Role": 3
     },
     {
        "Id": 4,
        "Name": "Mary Manager",
        "Role": 2
     },
     {
        "Id": 5,
        "Name": "Steve Trainer",
        "Role": 5
     }
]
JSON;

    /**
     * Stores the value of json_decoded $usersJson
     * @var array
     */
    private $users;

    /**
     * Stores the value of json_decoded $rolesJson
     * @var array
     */
    private $roles;

    /**
     * Convert JSON strings to user & role arrays
     */
    public function setUp() : void {
        $this->roles = json_decode(self::$rolesJson, true);
        $this->users = json_decode(self::$usersJson, true);
    }

    /**
     * Testing getSubOrdinates with the admin user.
     * This user has all other users below it.
     *
     * This matches example provided in test.
     */
    public function testGetSubOrdinatesWithAdminUser() : void {
        $userHierarchy = new UserHierarchy();
        $userHierarchy->setRoles($this->roles)
            ->setUsers($this->users);

        $users = $userHierarchy->getSubOrdinates(1);

        $this->assertCount(4, $users);

        list($user1, $user2, $user3, $user4) = $users;

        $this->assertEquals(4, $user1->getId());
        $this->assertEquals('Mary Manager', $user1->getName());
        $this->assertEquals(2, $user1->getRole());

        $this->assertEquals(3, $user2->getId());
        $this->assertEquals('Sam Supervisor', $user2->getName());
        $this->assertEquals(3, $user2->getRole());

        $this->assertEquals(2, $user3->getId());
        $this->assertEquals('Emily Employee', $user3->getName());
        $this->assertEquals(4, $user3->getRole());

        $this->assertEquals(5, $user4->getId());
        $this->assertEquals('Steve Trainer', $user4->getName());
        $this->assertEquals(5, $user4->getRole());
    }

    /**
     * Testing getSubOrdinates with a real user ID
     *
     * This matches example provided in test.
     */
    public function testGetSubOrdinatesUserId() : void {
        $userHierarchy = new UserHierarchy();
        $userHierarchy->setRoles($this->roles)
            ->setUsers($this->users);

        $users = $userHierarchy->getSubOrdinates(3);

        $this->assertCount(2, $users);

        $user1 = $users[0];
        $user2 = $users[1];
        $this->assertEquals(2, $user1->getId());
        $this->assertEquals('Emily Employee', $user1->getName());
        $this->assertEquals(4, $user1->getRole());

        $this->assertEquals(5, $user2->getId());
        $this->assertEquals('Steve Trainer', $user2->getName());
        $this->assertEquals(5, $user2->getRole());
    }

    /**
     * This method will test the mapOrdinates method with No parentRole.
     * It should return a single result based on the original data and the
     * user details should match that of the JSON above.
     */
    public function testMapSubOrdinatesWithNoRole() : void {
        $userHierarchy = new UserHierarchy();
        $userHierarchy->setRoles($this->roles)
            ->setUsers($this->users);

        // Testing with no value.
        $users = $userHierarchy->mapSubOrdinates();
        $this->assertTrue(!empty($users));
        $user = reset($users);

        $this->assertInstanceOf(UserInterface::class, $user);
        $this->assertEquals(1, $user->getId());
        $this->assertEquals('Adam Admin', $user->getName());
        $this->assertEquals(1, $user->getRole());
    }

    /**
     * This method will test the mapOrdinates method with a VALID parentRole.
     * It should return a single row and match the details in the JSON above.
     *
     * This also matches example provided in scope.
     */
    public function testMapSubOrdinatesWithValidRole() : void {
        $userHierarchy = new UserHierarchy();
        $userHierarchy->setRoles($this->roles)
            ->setUsers($this->users);

        $users = $userHierarchy->mapSubOrdinates(3);
        $this->assertCount(2, $users);

        list($user1, $user2) = $users;
        $this->assertEquals(2, $user1->getId());
        $this->assertEquals('Emily Employee', $user1->getName());
        $this->assertEquals(4, $user1->getRole());

        $this->assertEquals(5, $user2->getId());
        $this->assertEquals('Steve Trainer', $user2->getName());
        $this->assertEquals(5, $user2->getRole());
    }

    /**
     * This method will test the mapOrdinates method with an INVALID parentRole.
     * It won't be found and therefore should return an empty array
     */
    public function testMapSubOrdinatesWithInvalidRole() : void {
        $userHierarchy = new UserHierarchy();
        $userHierarchy->setRoles($this->roles)
            ->setUsers($this->users);

        $users = $userHierarchy->mapSubOrdinates(999);
        $this->assertEmpty($users);
    }

    /**
     * Find admin user.
     * @throws DuplicateUserException
     * @throws UserNotFoundException
     */
    public function testFindUserById() {
        $userHierarchy = new UserHierarchy();
        $userHierarchy->setRoles($this->roles)
            ->setUsers($this->users);

        $user = $userHierarchy->findUserById(1);

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals(1, $user->getId());
        $this->assertEquals('Adam Admin', $user->getName());
        $this->assertEquals(1, $user->getRole());
    }

    /**
     * Testing with a fake user ID.
     * We expect this to throw an exception.
     * @throws DuplicateUserException
     * @throws UserNotFoundException
     */
    public function testFindUserByIdWithFakeUserId() : void {
        $userHierarchy = new UserHierarchy();
        $userHierarchy->setRoles($this->roles)
            ->setUsers($this->users);

        $this->expectException(UserNotFoundException::class);
        $userHierarchy->findUserById(100);
    }

    /**
     * Testing with Dupe user IDs.
     * @throws DuplicateUserException
     * @throws UserNotFoundException
     */
    public function testFindUserByIdWithDuplicateUserId() : void {
        // Mock up a duplicate user.
        $this->users[] = [
            'Id' => 4,
            'Name' => 'Duplicate User',
            'Role' => 3
        ];

        $userHierarchy = new UserHierarchy();
        $userHierarchy->setRoles($this->roles)
            ->setUsers($this->users);

        $this->expectException(DuplicateUserException::class);
        $userHierarchy->findUserById(4);
    }

    /**
     * Test with valid role
     */
    public function testLookupRolesByParent() {
        $userHierarchy = new UserHierarchy();
        $userHierarchy->setRoles($this->roles)
            ->setUsers($this->users);

        $roles = $userHierarchy->lookupRolesByParent(1);

        $this->assertCount(1, $roles);

        $role = $roles[0];
        $this->assertEquals(2, $role->getId());
        $this->assertEquals('Location Manager', $role->getName());
        $this->assertEquals(1, $role->getParent());
    }

    /**
     * Test with invalid role parent.
     */
    public function testLookupRolesByParentInvalidParent() {
        $userHierarchy = new UserHierarchy();
        $userHierarchy->setRoles($this->roles)
            ->setUsers($this->users);

        $roles = $userHierarchy->lookupRolesByParent(500);

        $this->assertEmpty($roles);
    }

    /**
     * Test with valid role
     */
    public function testLookupUsersByRole() {
        $userHierarchy = new UserHierarchy();
        $userHierarchy->setRoles($this->roles)
            ->setUsers($this->users);

        $users = $userHierarchy->lookupUsersByRole(1);

        $this->assertCount(1, $users);

        $user = $users[0];
        $this->assertEquals(1, $user->getId());
        $this->assertEquals('Adam Admin', $user->getName());
        $this->assertEquals(1, $user->getRole());
    }

    /**
     * Test with invalid role
     */
    public function testLookupUsersByRoleWithInvalidRole() {
        $userHierarchy = new UserHierarchy();
        $userHierarchy->setRoles($this->roles)
            ->setUsers($this->users);

        $users = $userHierarchy->lookupUsersByRole(999);

        $this->assertEmpty($users);
    }
}
