<?php

namespace Polk;

interface UserInterface {
    /**
     * Returns a recursive list of user's child users.
     * @return array
     */
    public function getSubOrdinates() : array;
}