<?php

declare(strict_types=1);

namespace Polk;

/**
 * This class defines users hierarchy by mapping users to their
 * role and then assigning the users a list of subusers that are
 * beneath that role.
 * Class UserHierarchy
 * @package Polk
 */
class UserHierarchy {
    /**
     * @var array
     */
    protected $roles;

    /**
     * @var array
     */
    protected $users;

    /**
     * @var bool
     */
    protected $userRoleMapChanged = false;

    /**
     * @param array $roles
     * @return UserHierarchy
     */
    public function setRoles(array $roles) : self {
        $this->roles = array_map(function($role) {
            return new Role($role);
        }, $roles);

        $this->userRoleMapChanged = true;

        return $this;
    }

    /**
     * @param array $users
     * @return UserHierarchy
     */
    public function setUsers(array $users) : self {
        $this->users = array_map(function($user) {
            return new User($user);
        }, $users);

        $this->userRoleMapChanged = true;

        return $this;
    }

    /**
     * Loads roles and users with JSON provided.
     * @param string $roles
     * @param string $users
     * @return UserHierarchy
     */
    public function loadFromJSON(string $roles, string $users) : self {
        $this->setRoles(json_decode($roles, true));
        $this->setUsers(json_decode($users, true));

        return $this;
    }

    /**
     * This method loops through each role and sets assigns
     * each user object it's list of sub users.
     * @param int $parentId
     * @return array
     */
    public function mapSubOrdinates($parentId = 0) : array {
        // Stores a list of updated user objects
        $users = [];

        // Find role with Parent of 0.
        $roles = $this->lookupRolesByParent($parentId);

        // If there are no child roles return blank array.
        if (empty($roles)) {
            return [];
        }

        // Loop through roles and find users for each role.
        foreach ($roles as $role) {
            $users = array_merge($users, $this->lookupUsersByRole($role->getId()));

            // Loop through each user and find their sub users based on
            // the child role ID.
            foreach ($users as $user) {
                $user->setUsers($this->mapSubOrdinates($role->getId()));
            }
        }



        return array_values($users);
    }

    /**
     * Finds a list of roles by their parent role.
     * @param $parentId
     * @return array
     */
    public function lookupRolesByParent($parentId) {
        return array_values(array_filter($this->roles, function($role) use ($parentId) {
            return $role->getParent() === $parentId;
        }));
    }

    /**
     * Finds a list of users based on their roleId
     * This can return an empty array
     * @param $roleId
     * @return array
     */
    public function lookupUsersByRole($roleId) : array {
        return array_values(array_filter($this->users, function($user) use ($roleId) {
            return $user->getRole() === $roleId;
        }));
    }

    /**
     * Finds a user with a given userId or throws an exception.
     * @param $userId
     * @return UserInterface
     * @throws DuplicateUserException
     * @throws UserNotFoundException
     */
    public function findUserById($userId) : UserInterface {
        $users = array_values(array_filter($this->users, function($user) use ($userId) {
            return $user->getId() == $userId;
        }));

        if (empty($users)) {
            throw new UserNotFoundException($userId);
        }

        if (count($users) > 1) {
            throw new DuplicateUserException($userId);
        }

        return $users[0];
    }

    /**
     * Returns a list of ALL user's subordinates and their subordinates.
     * @param int $userId
     * @return array
     * @throws DuplicateUserException
     * @throws UserNotFoundException
     */
    public function getSubOrdinates(int $userId) : array {
        // Check to see whether the rollMapping needs to be configured again.
        // This property ges updated when ever a user or role is changed.
        if ($this->userRoleMapChanged) {
            $this->mapSubOrdinates();

            $this->userRoleMapChanged = false;
        }

        return $this->findUserById($userId)->getSubOrdinates();
    }
}