<?php

namespace Polk;

class DuplicateUserException extends \Exception {
    public function __construct($userId) {
        parent::__construct("Multiple users found with ID $userId");
    }
}