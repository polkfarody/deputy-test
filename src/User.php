<?php

namespace Polk;

use JsonSerializable;

class User implements UserInterface, JsonSerializable {

    /**
     * @var int
     */
    protected $id;
    /**
     * @var int
     */
    protected $role;
    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $users = [];

    /**
     * User constructor.
     * @param array $userObj
     */
    public function __construct(array $userObj) {
        $this->load($userObj);
    }

    /**
     * Populates this object using associative array.
     * @param array $userObj
     */
    public function load(array $userObj) {
        $this->id = $userObj['Id'];
        $this->name = $userObj['Name'];
        $this->role = $userObj['Role'];
    }

    /**
     * @inheritdoc
     */
    public function getSubOrdinates(): array {
        $subUsers = $this->users;
        foreach ($this->users as $user) {
            $subUsers = array_merge($subUsers, $user->getSubOrdinates());
        }

        return $subUsers;
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     * @return User
     */
    public function setId(int $id): User {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getRole(): int {
        return $this->role;
    }

    /**
     * @param int $role
     * @return User
     */
    public function setRole(int $role): User {
        $this->role = $role;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getUsers(): array {
        return $this->users;
    }

    /**
     * @param array $users
     * @return User
     */
    public function setUsers(array $users): User {
        $this->users = $users;
        return $this;
    }

    public function jsonSerialize() {
        return [
            'Id' => $this->id,
            'Name' => $this->name,
            'Role' => $this->role
        ];
    }
}