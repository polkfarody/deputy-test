<?php

namespace Polk;

class Role {
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $parent;

    /**
     * Role constructor.
     * @param array $roleObj
     */
    public function __construct(array $roleObj = []) {
        $this->load($roleObj);
    }

    /**
     * Populates this object using associative array.
     * @param $roleObj
     */
    public function load($roleObj) : void {
        $this->id = $roleObj['Id'];
        $this->name = $roleObj['Name'];
        $this->parent = $roleObj['Parent'];
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Role
     */
    public function setId(int $id): Role {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Role
     */
    public function setName(string $name): Role {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getParent(): int {
        return $this->parent;
    }

    /**
     * @param int $parent
     * @return Role
     */
    public function setParent(int $parent): Role {
        $this->parent = $parent;
        return $this;
    }
}