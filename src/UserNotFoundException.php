<?php

namespace Polk;

class UserNotFoundException extends \Exception {
    public function __construct($userId) {
        parent::__construct("User with ID $userId was not found");
    }
}